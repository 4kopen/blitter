/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <stdbool.h>
#include <stdint.h>
#include <linux/stm/bdisp2_ioctl.h>
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define USEC_PER_SEC 1000000
# define TIMESPEC_TO_TIMEVAL(tv, ts) {                              \
    (tv)->tv_sec = (ts)->tv_sec;                                    \
    (tv)->tv_usec = (ts)->tv_nsec / 1000;                           \
}

int
main (int argc, char *argv[])
{
  struct timespec current;
  struct timeval  oldtime[2];

  int fd_k = -1, fd_u = -1;

  /* for a description of these names, have a look in the stgfx2 source code
     in stm-gfxdriver.c */
  /* device names as used by stgfx2 */
  static const char *devnames_u[] = {
    "/dev/stm-bdispII.1.1", /* STiH407 */
  };
  /* devices used inside the kernel */
  static const char *devnames_k[] = {
    "/dev/stm-bdispII.0.0", /* STiH407 */
    NULL,
  };
  unsigned int i;

  for (i = 0; i < sizeof(devnames_u) / sizeof(devnames_u[0]); ++i)
    {
      fd_u = open (devnames_u[i], O_RDONLY);
      if (fd_u == -1)
        continue;
      fd_k = open (devnames_k[i], O_RDONLY);
      break;
    }

  if (fd_u == -1)
    fd_u = open ("/dev/fb0", O_RDONLY);

  if (fd_u == -1)
    {
      fprintf (stderr, "Could not open blitter device\n");
      exit (1);
    }

  clock_gettime (CLOCK_MONOTONIC, &current);
  TIMESPEC_TO_TIMEVAL (&oldtime[0], &current);
  oldtime[1] = oldtime[0];

  while (1)
    {
      int i;
      int fds[2] = { fd_u, fd_k };
      char buf[100];
      int pos = 0;

      for (i = 0; (i < 2) && (fds[i] != -1); ++i)
        {
          unsigned long load1;
          unsigned long long load2 = -1;
          unsigned long long ticks_past;
          struct timeval currenttime, difftime;

          if (ioctl (fds[i], STM_BLITTER_GET_BLTLOAD2, &load2) != 0
              && i == 0)
            {
              /* fallback for stmfb */
              if (ioctl (fds[i], STM_BLITTER_GET_BLTLOAD, &load1) == 0)
                load2 = load1;
            }

          if (load2 != -1)
            {
//              printf ("load %llu\n", load2);
              clock_gettime (CLOCK_MONOTONIC, &current);
              TIMESPEC_TO_TIMEVAL (&currenttime, &current);
              timersub (&currenttime, &oldtime[i], &difftime);

              ticks_past = (difftime.tv_sec * USEC_PER_SEC + difftime.tv_usec);
              oldtime[i] = currenttime;

              pos += snprintf (&buf[pos], sizeof (buf) - pos,
                               "past[%c]: %6llu -> stat: %8llu  ", (i == 0) ? 'u' : 'k', ticks_past, load2);
              pos += snprintf (&buf[pos], sizeof (buf) - pos,
                               "load %3.2f%%    ", (double) (load2 * 100) / ticks_past);
            }
          printf ("%s\r", buf);
          usleep (100 * 1000);
        }
    }

  return 0;
}
