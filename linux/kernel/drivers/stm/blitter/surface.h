/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __SURFACE_H__
#define __SURFACE_H__

#ifdef __KERNEL__
#include <linux/kref.h>
#endif /* __KERNEL__ */

#include <linux/stm/blitter.h>
#include "state.h"


struct stm_blitter_surface_s {
#ifdef __KERNEL__
	struct kref kref;
	unsigned int magic;
#endif /* __KERNEL__ */

	stm_blitter_surface_format_t     format;
	stm_blitter_surface_colorspace_t colorspace;

	stm_blitter_surface_address_t    buffer_address;
	unsigned long                    buffer_size;

	stm_blitter_dimension_t          size;
	unsigned long                    stride;

	stm_blitter_color_t              src_ckey[2]; /* low and high */
	stm_blitter_colorkey_mode_t      src_ckey_mode;

	unsigned int creation; /* serial number of surface/state */
	struct stm_blitter_state state;

	stm_blitter_serial_t serial; /*!< serial number of last operation on
					  this surface */
};


#endif /* __SURFACE_H__ */
