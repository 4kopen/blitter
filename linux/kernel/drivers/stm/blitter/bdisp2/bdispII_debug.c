/*
* This file is dual licensed, either GPLv2.0
* or LGPLv2.1, at your option.
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
********************************************************************************
*
* License type: GPLv2.0
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with it. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
* Alternatively, this may be distributed under the terms of
* LGPLv2.1, in which case the following provisions apply instead of the ones
* mentioned above :
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
*/

#include "blitter_os.h"

#include "bdisp2/bdispII_aq_state.h"
#include "bdisp2/bdispII_debug.h"

const char *
bdisp2_get_palette_type_name(enum bdisp2_palette_type palette)
{
#define CMP(pal) case pal: return #pal
	switch (palette) {
	CMP(SG2C_NORMAL);
	CMP(SG2C_COLORALPHA);
	CMP(SG2C_INVCOLORALPHA);
	CMP(SG2C_ALPHA_MUL_COLORALPHA);
	CMP(SG2C_INV_ALPHA_MUL_COLORALPHA);
	CMP(SG2C_ONEALPHA_RGB);
	CMP(SG2C_INVALPHA_ZERORGB);
	CMP(SG2C_ALPHA_ZERORGB);
	CMP(SG2C_ZEROALPHA_RGB);
	CMP(SG2C_ZEROALPHA_ZERORGB);
	CMP(SG2C_ONEALPHA_ZERORGB);

	case SG2C_COUNT:
	default:
		break;
	}

	return "unknown";
#undef CMP
};
