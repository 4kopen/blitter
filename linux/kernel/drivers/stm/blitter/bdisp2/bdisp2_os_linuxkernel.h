/*
* This file is dual licensed, either GPLv2.0
* or LGPLv2.1, at your option.
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
********************************************************************************
*
* License type: GPLv2.0
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with it. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
* Alternatively, this may be distributed under the terms of
* LGPLv2.1, in which case the following provisions apply instead of the ones
* mentioned above :
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
*/

#ifndef __BDISP2_OS_LINUXKERNEL_H__
#define __BDISP2_OS_LINUXKERNEL_H__

#include <linux/kernel.h>
#include <linux/bug.h>
#include <linux/slab.h>

#define STM_BLITTER_DEBUG_DOMAIN(domain, str, desc) \
		static int __maybe_unused domain

#define THIS_FILE ((strrchr(__FILE__, '/') ?: __FILE__ - 1) + 1)

/*
 * STM_BLITTER_ASSERT has to be used to track only development bug likely
 * impossible situations and not wrong parameter. Could be evaluated to void
 * if debug is disabled so has to be used with and braces when needed.
 */
#ifndef STM_BLITTER_ASSERT
#  define STM_BLITTER_ASSERT(cond) BUG_ON(!(cond))
#endif
#define STM_BLITTER_ASSUME(cond) WARN_ON(!(cond))
#define stm_blit_printw(format, args...) \
		printk(KERN_WARNING \
		       "%s:%d: " format, THIS_FILE, __LINE__, ##args)
#define stm_blit_printe(format, args...) \
		printk(KERN_ERR "%s:%d: " format, THIS_FILE, __LINE__, ##args)
#define stm_blit_printi(format, args...) \
		printk(KERN_INFO "%s:%d: " format, THIS_FILE, __LINE__, ##args)
#ifdef DEBUG
#define stm_blit_printd(domain, format, args...) \
		printk(KERN_DEBUG "%s:%d: " format, THIS_FILE, __LINE__, ##args)
#else
#define stm_blit_printd(domain, format, args...) do { } while (0)
/*
static inline int __printf(2, 3)
stm_blit_printd(int level, const char *format, ...)
{
	return 0;
}
*/
#endif


#define STM_BLITTER_N_ELEMENTS(array) ARRAY_SIZE(array)
#define STM_BLITTER_ABS(val)          abs(val)
#define STM_BLITTER_MIN(a, b)         min(a, b)

#define STM_BLITTER_UDELAY(usec)                  udelay(usec)
#define STM_BLITTER_RAW_WRITEL(val, addr)         __raw_writel(val, addr)
#define STM_BLITTER_MEMSET_IO(dst, val, size)     memset_io(dst, val, size)
#define STM_BLITTER_MEMCPY_TO_IO(dst, src, size)  memcpy_toio(dst, src, size)


static inline void
flush_bdisp2_dma_area_region(struct stm_bdisp2_dma_area *area,
			     unsigned                    offset,
			     unsigned                    size)
{
	if (!area->cached)
		return;

	/* We only need to flush the cache for the node lists. They should
	 * always be mapped write combined for the ARM, so we do not need to
	 * flush them, and it is a bug if we do as the function does not exist
	 * on the ARM
	 */
#ifdef CONFIG_SH
	flush_ioremap_region(area->base, area->memory, offset, size);
#else
	BUG();
#endif
}

static inline void flush_bdisp2_dma_area(struct stm_bdisp2_dma_area *area)
{
	flush_bdisp2_dma_area_region(area, 0, area->size);
}


uint64_t
bdisp2_div64_u64 (uint64_t n,
		  uint64_t d);

/*
 * Using udelay(1) to acheive the wait allows high response once the
 * lock is freed. But is keeping CPU active is a busy wait. This works
 * fine with normal thread, except keeping CPU busy for nothing.
 *
 * For RT thread like Scaler, this results in RT thread being blocked
 * in the busy wait and monopolizing the CPU usage blocking event the
 * lock holder from releasing it.
 *
 * Away to avoid that is to use "non-busy" wait by using usleep_range
 * function as replacement. Still usleep_range has a draw-back of may
 * generate an interruption and could results in performance drop duo
 * to high number of introduced frequency.
 *
 * Best approach will be to use udelay(1) for normal thread, as today,
 * and use usleep_range for RT thread to allow other thread to proceed.
 * Unfortunately, from kernel module there is no viable wayto detect
 * current thread scheduling priority.
 *
 * To bypass this limitation, wait for 10 time udelay(1). If we are
 * still there means propably we are in a real time thread, so use
 * usleep_range(1,20), to give time to lock holder to release it. 20 is
 * enough big to prepare more than 6 nodes, so lock will propably be
 * unlocked. And not so big to avoid RT thread performance penalty.
 */

#  define LOCK_ATOMIC_RPM(stdrv,stdev) \
	({ \
		int wait_counter=10; \
		while (unlikely(atomic_cmpxchg(&((stdev)->bdisp_nodes.lock), 0, 1) != 0))   \
		{ \
			if ( (stdrv)->bdisp_suspended == 2) \
				bdisp2_wakeup_os((stdrv)); \
			if (wait_counter-->=0) \
				udelay(1); \
			else \
				usleep_range(1,20); \
		} \
	})

#  define LOCK_ATOMIC(stdev) \
	({ \
		int wait_counter=10; \
		while (unlikely(atomic_cmpxchg(&((stdev).bdisp_nodes.lock), 0, 1) != 0))   \
		{ \
			if (wait_counter-->=0) \
				udelay(1); \
			else \
				usleep_range(1,20); \
		} \
	})

#  define UNLOCK_ATOMIC(stdev) \
	({ \
		atomic_set(&((stdev)->bdisp_nodes.lock), 0);                                \
	})

#  define BDISP2_WMB() smp_wmb()
#  define BDISP2_MB()  smp_mb()

#define bdisp2_start_timer_os() \
	({ \
		ktime_to_us(ktime_get()); \
	})

#include <linux/io.h>

static inline uint32_t
bdisp2_get_reg(const struct stm_bdisp2_driver_data * const stdrv,
	       uint16_t                             offset)
{
	return readl(stdrv->mmio_base + 0xa00 + offset);
}

static inline void
bdisp2_set_reg(struct stm_bdisp2_driver_data * const stdrv,
	       uint16_t                       offset,
	       uint32_t                       value)
{
/*
	__raw_writel(value, stdrv->mmio_base + 0xa00 + offset);
*/
	writel(value, stdrv->mmio_base + 0xa00 + offset);
}

int
bdisp2_wakeup_os(struct stm_bdisp2_driver_data * const stdrv);

#endif /* __BDISP2_OS_LINUXKERNEL_H__ */
