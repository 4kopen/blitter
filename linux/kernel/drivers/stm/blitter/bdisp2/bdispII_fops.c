/*
* This file is dual licensed, either GPLv2.0
* or LGPLv2.1, at your option.
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
********************************************************************************
*
* License type: GPLv2.0
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with it. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
* Alternatively, this may be distributed under the terms of
* LGPLv2.1, in which case the following provisions apply instead of the ones
* mentioned above :
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
*/

#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/version.h>
#include <linux/hrtimer.h>
#include <linux/stat.h>
#include <linux/debugfs.h>
#include <linux/io.h>
#include <linux/uaccess.h>

#include <linux/stm/bdisp2_ioctl.h>
#include "bdisp2/bdispII_fops.h"
#include "bdisp2/bdispII_debugfs.h"
#include "bdisp2/bdispII_aq.h"
#include "bdisp2_os_linuxkernel.h"
#include "bdisp2/bdisp_accel.h"
#include "bdisp2/bdisp2_os.h"
static int
stm_bdisp2_open(const struct device *sbcd_device)
{
	return 0;
}

static void
stm_bdisp2_close(const struct device *sbcd_device) { }


static void
stm_blitter_iomm_vma_open(struct vm_area_struct * const vma)
{
	struct stm_bdisp2_aq * const aq = vma->vm_private_data;
	unsigned long physaddr = (vma->vm_pgoff << PAGE_SHIFT);

	if (physaddr == aq->bc->mem_region->start)
		atomic_inc(&aq->n_users);
}

static void
stm_blitter_iomm_vma_close(struct vm_area_struct * const vma)
{
	struct stm_bdisp2_aq * const aq = vma->vm_private_data;
	unsigned long physaddr = (vma->vm_pgoff << PAGE_SHIFT);

	if (physaddr == aq->bc->mem_region->start)
		atomic_dec(&aq->n_users);

}

static int
stm_blitter_iomm_vma_fault(struct vm_area_struct *vma,
			   struct vm_fault       *vmf)
{
	/* we want to provoke a bus error rather than give the client
	   the zero page */
	return VM_FAULT_SIGBUS;
}

static const struct vm_operations_struct stm_blitter_iomm_nopage_ops = {
	.open  = stm_blitter_iomm_vma_open,
	.close = stm_blitter_iomm_vma_close,
	.fault = stm_blitter_iomm_vma_fault,
};

#define _LOCAL_VM_FLAGS		(VM_IO | VM_DONTDUMP | VM_DONTEXPAND)

static int
stm_bdisp2_mmap(const struct device   *sbcd_device,
		struct vm_area_struct *vma)
{

	return -EPERM;
}


static long
stm_bdisp2_ioctl(const struct device *sbcd_device,
		 unsigned int         cmd,
		 unsigned long        arg)
{
	struct stm_bdisp2_aq * const aq = dev_get_drvdata(sbcd_device);
	struct stm_bdisp2_driver_data * const stdrv = &aq->stdrv;
	struct stm_bdisp2_device_data * const stdev = &aq->stdev;

	switch (cmd) {

	case STM_BLITTER_SYNC:
		{
		int res = 0;
		struct wait_cmd param;
		enum stm_bdisp2_wait_type wait;

		if (copy_from_user(&param, (void *) arg, sizeof(struct wait_cmd))) {
			return -EFAULT;
		}

		wait = (param.wait == STM_BLITTER_W_IDLE) ? STM_BDISP2_WT_IDLE : STM_BDISP2_WT_FENCE;

		if (!bdisp2_is_idle(stdrv))
		{
			res = stm_bdisp2_aq_sync(aq, wait, &param.serial);
			if (res != 0)
				printk(KERN_DEBUG "sync returned %d\n", res);
		}
		return res;
		}
		break;

	case STM_BLITTER_GET_BLTLOAD:
	case STM_BLITTER_GET_BLTLOAD2:
		{
		unsigned long long busy;

		/* we don't do any locking here - this all is for statistics
		   only anyway... */
		if (stdev->bdisp_ops_start_time) {
			/* normally, we calculate the busy ticks upon a node
			   interrupt. But if GET_BLTLOAD is called more often
			   than interrupts occur, it will return zero for a
			   very long time, and then all of a sudden a huge(!)
			   value for busy ticks. To prevent this from
			   happening, we calculate the busy ticks here as
			   well. */
			unsigned long long nowtime = ktime_to_us(ktime_get());
			stdev->ticks_busy
				+= (nowtime
				    - stdev->bdisp_ops_start_time
				   );
			stdev->bdisp_ops_start_time = nowtime;
		}

		busy = stdev->ticks_busy;
		stdev->ticks_busy = 0;

		if (cmd == STM_BLITTER_GET_BLTLOAD)
			return put_user((unsigned long) busy,
					(unsigned long *) arg);

		return put_user(busy, (unsigned long long *) arg);
		}
		break;
	case STM_BLITTER_GET_SERIAL:
		{
		struct get_serial_cmd param;

		bdisp2_get_serial(stdrv,stdev,&param.serial);

		if (copy_to_user((void *) arg, &param, sizeof(struct get_serial_cmd))) {
			return -EFAULT;
		}
		return 0;
		}
		break;
	case STM_BLITTER_PUSH_NODES:
		{
		int res;
		struct push_nodes_cmd cmd;
		unsigned char *read_p,*write_p;
		int node_size;
		res = 0;

		if (copy_from_user(&cmd,(void *) arg, sizeof(struct push_nodes_cmd))){
			printk(KERN_ERR "%s copy_from_user error \n",__func__);
			return -EFAULT;
		}

		if ((cmd.nodes_buf_len < 0) || (cmd.nodes_buf_len > STM_GFXDRV_NODES_BUF_SZ))
			return -EINVAL;

#ifdef DEBUG
		printk("%s  STM_BLITTER_PUSH_NODES cmd.nodes_buf_p %p \
				cmd.nodes_buf_len %d cmd.emit %d\n ",
				__func__,cmd.nodes_buf_p,cmd.nodes_buf_len,cmd.emit);
#endif
		/* Copy node by node from cmd to the node list red by Hw. */

		read_p = (unsigned char *) cmd.nodes_buf_p;

		/* warning : keep the wile loop optimized
		 * as much as possible for the perfs */
		while (cmd.nodes_buf_len > 0)
		{
			res = copy_from_user(&node_size,read_p, sizeof(uint32_t));

			if ((node_size < 0) || (node_size > sizeof(struct _BltNode_Complete)))
				return -EINVAL;

			read_p += sizeof(uint32_t);

			write_p = (unsigned char *) bdisp2_get_new_node(stdrv, stdev);

			/* Preserves the nip initialized at probe time. */
			res = copy_from_user(write_p + sizeof(uint32_t), read_p + sizeof(uint32_t), node_size-sizeof(uint32_t));

			bdisp2_update_num_ops(stdrv, stdev,
			&(((*(struct _BltNode_Complete *) write_p)).common.ConfigGeneral.BLT_INS));

			bdisp2_finish_node(stdrv, stdev, write_p, 0);

			read_p += node_size;
			cmd.nodes_buf_len -= (sizeof(uint32_t) + node_size);
		}

		if (cmd.emit == 1)
			bdisp2_emit_commands(stdrv, stdev, false);

		return res;
		}
		break;
	case STM_BLITTER_GET_FEATURES:
		{
		struct get_bdisp2_features_cmd cmd;

		if (bdisp2_init_features_by_type (&stdev->features, &stdrv->plat_blit))
		{
			printk(KERN_ERR "%s blitter is %d (unknown)\n",__func__,stdrv->plat_blit.device_type);
			return -EINVAL;
		}

		cmd.features = stdev->features;

		if (copy_to_user((void *) arg, &cmd ,sizeof(struct get_bdisp2_features_cmd))){
			printk(KERN_ERR "%s copy_to_user error \n",__func__);
			return -EFAULT;
		}
		return 0;
		}
		break;
	case STM_BLITTER_GET_DRV_VER:
		{
		return put_user(STM_BSISP2_DRV_VERSION, (int *) arg);
		}
		break;

	}
	return -ENOTTY;
}

const struct stm_blitter_file_ops bdisp2_aq_fops = {
	.open  = stm_bdisp2_open,
	.close = stm_bdisp2_close,

	.mmap  = stm_bdisp2_mmap,
	.ioctl = stm_bdisp2_ioctl,

	.debugfs = stm_bdisp2_add_debugfs,
};
