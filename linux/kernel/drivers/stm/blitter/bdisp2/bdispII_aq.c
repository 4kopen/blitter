/*
* This file is dual licensed, either GPLv2.0
* or LGPLv2.1, at your option.
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
********************************************************************************
*
* License type: GPLv2.0
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with it. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
* Alternatively, this may be distributed under the terms of
* LGPLv2.1, in which case the following provisions apply instead of the ones
* mentioned above :
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
*/

#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/delay.h>
#include <linux/hrtimer.h>
#include <linux/bpa2.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/pm.h>
#include <linux/pm_runtime.h>

#include <asm/sizes.h>

#if defined(CONFIG_ARM)
/* ARM has mis-named this interface relative to all the other platforms that
   define it (including x86). */
#  define ioremap_cache ioremap_cached
#endif

#include "linux/stm/blitter.h"
#include <linux/stm/bdisp2_nodegroups.h>
#include <linux/stm/bdisp2_registers.h>

#include "blit_debug.h"

#include "bdisp2/bdispII_aq.h"
#include "bdisp2/bdisp_accel.h"

#include "bdisp2/bdisp2_os.h"


static const int NODES_SIZE = 45 * PAGE_SIZE;


#define blitload_init_ticks(stdev) \
	({ \
	(stdev).bdisp_ops_start_time = 0; \
	(stdev).ticks_busy = 0; \
	})

/* we want some more statistics, so lets remember the number of ticks the
   BDisp was busy executing nodes */
#define blitload_calc_busy_ticks(stdev) \
	({ \
	ktime_t now = ktime_get(); \
	(stdev).ticks_busy += (ktime_to_us(now)                               \
			       - (stdev).bdisp_ops_start_time);               \
	if (unlikely(!(stdev).ticks_busy))                                    \
		/* this can happen if the BDisp was faster in finishing a     \
		   Node(list) than it takes us to see a change in 'ticks'.    \
		   'Ticks' are in useconds at the moment, so fast enough; but \
		   still. Lets assume the BDisp was busy, because else the    \
		   interrupt should not have been asserted! */                \
		(stdev).ticks_busy = 1;                                       \
		(stdev).bdisp_ops_start_time = 0;                                     \
	})


int
stm_bdisp2_aq_sync(struct stm_bdisp2_aq       *aq,
		   enum stm_bdisp2_wait_type   wait,
		   const stm_blitter_serial_t * const serial)
{
	int res;

	/* We need to deal with possible signal delivery while sleeping, this is
	   indicated by a non zero return value.
	   The sleep itself is safe from race conditions */
	stm_blit_printd(3,
			"%p: Waiting for %s.... (%s, last_free/next_free %lu-%lu, CTL %.8x, STA %.8x ITS %.8x)\n",
			aq,
			((wait == STM_BDISP2_WT_IDLE)
			 ? "idle"
			 : ((wait == STM_BDISP2_WT_SPACE)
			    ? "a bit"
			    : "fence")),
			(!bdisp2_is_idle(&aq->stdrv)
			 ? "running"
			 : "   idle"),
			aq->stdev.bdisp_nodes.last_free,
			aq->stdev.bdisp_nodes.next_free,
			bdisp2_get_AQ_reg(&aq->stdrv, &aq->stdev,
					  BDISP_AQ_CTL),
			bdisp2_get_AQ_reg(&aq->stdrv, &aq->stdev,
					  BDISP_AQ_STA),
			bdisp2_get_reg(&aq->stdrv, BDISP_ITS));

	switch (wait) {
	case STM_BDISP2_WT_IDLE:
		/* doesn't need to be atomic, it's for statistics only
		   anyway... */
		aq->stdrv.stat.num_wait_idle++;

		res = wait_event_interruptible(
			aq->idle_wq,
			bdisp2_is_idle(&aq->stdrv));
		break;
	case STM_BDISP2_WT_SPACE:
		/* doesn't need to be atomic, it's for statistics only
		   anyway... */
		aq->stdrv.stat.num_wait_next++;

		res = wait_event_interruptible(
			aq->last_free_wq,
			(aq->stdev.bdisp_nodes.last_free
			 != aq->stdev.bdisp_nodes.next_free));
		break;
	case STM_BDISP2_WT_FENCE:
		{
		unsigned long lo = *serial & 0xffffffff;

		/* FIXME: add statistics for fence waits */

		res = wait_event_interruptible(
			aq->last_free_wq,
			(bdisp2_is_idle(&aq->stdrv)
			 || (bdisp2_get_reg(&aq->stdrv,
					    0x200 + 0xb4 /* BLT_USR */)
			     >= lo)));
		}
		break;
	default:
		/* Should never been reached */
		BUG();
		res = 0;
		break;
	}

	stm_blit_printd(3,
			"%p: ...done (%s, last_free/next_free %lu-%lu, CTL %.8x, STA %.8x ITS %.8x)\n",
			aq,
			(!bdisp2_is_idle(&aq->stdrv)
			 ? "running"
			 : "   idle"),
			aq->stdev.bdisp_nodes.last_free,
			aq->stdev.bdisp_nodes.next_free,
			bdisp2_get_AQ_reg(&aq->stdrv, &aq->stdev,
					  BDISP_AQ_CTL),
			bdisp2_get_AQ_reg(&aq->stdrv, &aq->stdev,
					  BDISP_AQ_STA),
			bdisp2_get_reg(&aq->stdrv, BDISP_ITS));

	return res;
}

static irqreturn_t
stm_bdisp2_aq_interrupt(int   irq,
			void *data)
{
	struct stm_bdisp2_aq *aq = data;
	unsigned long flags, irqmask;


	/* check and clear interrupt bits for this queue */
	spin_lock_irqsave(&aq->bc->register_lock, flags);
	irqmask = bdisp2_get_reg(&aq->stdrv, BDISP_ITS) & (aq->lna_irq_bit
							   | aq->node_irq_bit);
	bdisp2_set_reg(&aq->stdrv, BDISP_ITS, irqmask);
	spin_unlock_irqrestore(&aq->bc->register_lock, flags);

#if defined(CONFIG_PM_RUNTIME)
	/* Mark current time as being last busy */
	pm_runtime_mark_last_busy(aq->dev);

	if (aq->stdrv.num_pending_requests == 0) {
		aq->stdrv.num_pending_requests++;

		/* Call for autosuspend */
		pm_runtime_put_autosuspend(aq->dev);
	}
#endif

	/* check if interrupt was for us */
	if (irqmask) {
		++aq->stdrv.stat.num_irqs;

		/* the status register hold the address of the last node that
		   generated an interrupt */
		aq->stdev.bdisp_nodes.last_free = (bdisp2_get_AQ_reg(&aq->stdrv, &aq->stdev,
						       BDISP_AQ_STA)
			     - aq->stdev.bdisp_nodes.nodes_phys);

		stm_blit_printd(3, "%s @ %p: ITS: %.8lx: %c%c\n",
				__func__, aq, irqmask,
				(irqmask & aq->node_irq_bit) ? 'n' : '_',
				(irqmask & aq->lna_irq_bit) ?  'q' : '_');

		if (irqmask & aq->node_irq_bit) {
			ktime_t now = ktime_get();

			blitload_calc_busy_ticks(aq->stdev);
			aq->stdev.bdisp_ops_start_time = ktime_to_us(now);

			++aq->stdrv.stat.num_node_irqs;
			if (aq->stdrv.last_lut
			    && (aq->stdev.bdisp_nodes.last_free == aq->stdrv.last_lut))
				aq->stdrv.last_lut = 0;
		}

		if (irqmask & aq->lna_irq_bit) {
			/* We have processed all currently available nodes in
			   the buffer so signal that the blitter is now finished
			   for SyncChip. */
			/* It can happen that the IRQ was fired at the same time
			   as userspace was updating the LNA, in which case we
			   get the LNA interrupt and the BDisp is not idle
			   (anymore) as it's already executing the new
			   instruction(s). */
			if (!bdisp2_is_idle(&aq->stdrv)) {
				stm_blit_printd(
					3,
					"%s @ %p: atomic...: %.8lx/%.8x\n",
					__func__, aq,
					aq->stdev.bdisp_nodes.prev_set_lna,
					bdisp2_get_AQ_reg(&aq->stdrv,
							  &aq->stdev,
							  BDISP_AQ_LNA));
			} else {

				++aq->stdrv.stat.num_idle;

				stm_blit_printd(
					3,
					"%s @ %p: done: %.8lx/%.8x\n",
					__func__, aq,
					aq->stdev.bdisp_nodes.prev_set_lna,
					bdisp2_get_AQ_reg(&aq->stdrv,
							  &aq->stdev,
							  BDISP_AQ_LNA));

				blitload_calc_busy_ticks(aq->stdev);
			}

			++aq->stdrv.stat.num_lna_irqs;
			aq->stdrv.last_lut = 0;

			mb();

			wake_up_all(&aq->idle_wq);
		}

		mb();

		/* signal to sleepers that aq->shared->last_free has changed */
		wake_up_all(&aq->last_free_wq);
	}

	return IRQ_RETVAL(irqmask);
}


/* align addr on a size boundary - adjust address up/down if needed */
#define _ALIGN_UP(addr, size)   (((addr)+((size)-1))&(~((size)-1)))
#define _ALIGN_DOWN(addr, size) ((addr)&(~((size)-1)))

/* align addr on a size boundary - adjust address up if needed */
#define _ALIGN(addr, size)      _ALIGN_UP(addr, size)

int
stm_bdisp2_allocate_bpa2(size_t                      size,
			 uint32_t                    align,
			 bool                        cached,
			 struct stm_bdisp2_dma_area *area)
{
	static const char * const partnames[] = { "blitter" };
	unsigned int       idx; /* index into partnames[] */
	int                pages;

	BUG_ON(area == NULL);

	area->part = NULL;
	for (idx = 0; idx < ARRAY_SIZE(partnames); ++idx) {
		area->part = bpa2_find_part(partnames[idx]);
		if (area->part)
			break;
		stm_blit_printd(0,
				"BPA2 partition '%s' not found!\n",
				partnames[idx]);
	}
	if (!area->part) {
		stm_blit_printe("No BPA2 partition found!\n");
		goto out;
	}
	stm_blit_printd(0, "Using BPA2 partition '%s'\n", partnames[idx]);

	/* round size to pages */
	pages = (size + PAGE_SIZE - 1) / PAGE_SIZE;
	area->size = pages * PAGE_SIZE;

	area->base = bpa2_alloc_pages(area->part,
				      pages, align / PAGE_SIZE, GFP_KERNEL);
	if (!area->base) {
		stm_blit_printe(
			"Couldn't allocate %d pages from BPA2 partition '%s'\n",
			pages, partnames[idx]);
		goto out;
	}

	/* remap physical */
	if (cached)
		area->memory = ioremap_cache(area->base, area->size);
	else
		area->memory = ioremap_wc(area->base, area->size);
	if (!area->memory) {
		stm_blit_printe("ioremap of BPA2 memory failed\n");
		goto out_bpa2_free;
	}

	area->cached = cached;

	return 0;

out_bpa2_free:
	bpa2_free_pages(area->part, area->base);
out:
	return -ENOMEM;
}

void
stm_bdisp2_free_bpa2(struct stm_bdisp2_dma_area *area)
{
	BUG_ON(area == NULL);

	iounmap((void *) area->memory);
	bpa2_free_pages(area->part, area->base);

	area->part = NULL;
	area->base = 0;
	area->size = 0;
	area->memory = NULL;
}

int
stm_bdisp2_aq_init(struct stm_bdisp2_aq      *aq,
		   int                        index,
		   int                        irq,
		   struct stm_plat_blit_data *plat_data,
		   struct stm_bdisp2_config  *bc)
{
	int res;

	if (index >= STM_BDISP2_MAX_AQs)
		return -ENODEV;

	aq->bc = bc;

	aq->stdev.queue_id = index;
	aq->queue_prio = STM_BDISP2_MAX_AQs - index - 1;

	aq->stdrv.mmio_base = bc->io_base;
	aq->lna_irq_bit = (BDISP_ITS_AQ_LNA_REACHED
			   << (BDISP_ITS_AQ1_SHIFT + (index * 4)));
	aq->node_irq_bit = (BDISP_ITS_AQ_NODE_NOTIFY
			    << (BDISP_ITS_AQ1_SHIFT + (index * 4)));

	init_waitqueue_head(&aq->idle_wq);
	init_waitqueue_head(&aq->last_free_wq);
	atomic_set(&aq->n_users, 0);

	/* nodes */
	res = stm_bdisp2_allocate_bpa2(
			NODES_SIZE,
			(2*shm_align_mask) & ~shm_align_mask,
			false,
			&aq->stdev.bdisp_nodes.area);
	if (res) {
		stm_blit_printe("failed to allocate BDisp nodelist\n");
		return res;
	}

	aq->stdrv.version = STM_BSISP2_DRV_VERSION;

	aq->stdev.bdisp_nodes.nodes_size = aq->stdev.bdisp_nodes.area.size;
	aq->stdev.bdisp_nodes.nodes_phys = aq->stdev.bdisp_nodes.area.base;
	atomic_set(&aq->stdev.bdisp_nodes.lock, 0);

	aq->stdrv.plat_blit = *plat_data;

	blitload_init_ticks(aq->stdev);

	clk_prepare_enable(aq->bc->clk_ip);

	/* do a BDisp global soft reset on the first device */
	if (index == 0)
		bdisp2_engine_reset(&aq->stdrv, &aq->stdev);

	bdisp2_initialize(&aq->stdrv, &aq->stdev);

	stm_blit_printd(0,
			"%s @ %p: nodes: v/p/s: %p/%.8lx/%lu\n",
			__func__, aq,
			aq->stdev.bdisp_nodes.area.memory,
			aq->stdev.bdisp_nodes.nodes_phys,
			aq->stdev.bdisp_nodes.nodes_size);

	aq->irq = irq;
	/* not threaded for the time being */
	res = request_threaded_irq(aq->irq,
				   stm_bdisp2_aq_interrupt,
				   NULL,
				   IRQF_SHARED,
				   aq->stdrv.plat_blit.device_name,
				   aq);
	if (res < 0) {
		stm_blit_printe("Failed request_irq %d: %d!\n", aq->irq, res);
		aq->irq = 0;
		goto out_nodes;
	}

	return 0;

out_nodes:
	stm_bdisp2_free_bpa2(&aq->stdev.bdisp_nodes.area);

	return res;
}

void
stm_bdisp2_aq_release(struct stm_bdisp2_aq *aq)
{
	if (!aq->stdrv.bdisp_suspended)
		stm_bdisp2_aq_suspend(aq);

	bdisp2_cleanup(&aq->stdrv, &aq->stdev);

	free_irq(aq->irq, aq);
	stm_bdisp2_free_bpa2(&aq->stdev.bdisp_nodes.area);

}


/* Below is the proposed implementation with simple suspend/resume callbacks */


int
stm_bdisp2_aq_suspend(struct stm_bdisp2_aq *aq)
{
	int res;

	/* Lock the access while the blitter is suspended */
	LOCK_ATOMIC(aq->stdev);

	/* We should stop emitting commands before sync, otherwise the
	   driver would take a long time before going to suspend as it
	   may have some other emited commands to process. */
	aq->stdrv.bdisp_suspended = 1;
	res = stm_bdisp2_aq_sync(aq, STM_BDISP2_WT_IDLE, NULL);

	if (res != 0)
		printk(KERN_DEBUG "sync returned %d\n", res);

	/* Save AQ_IP register value to be restored after resume */
	aq->stdev.bdisp_nodes.next_ip = bdisp2_get_AQ_reg (&aq->stdrv, &aq->stdev, BDISP_AQ_IP);

	/* Disable the blitter IRQ */
	disable_irq(aq->irq);

	/* Disable the blitter clocks */
	clk_disable_unprepare(aq->bc->clk_ip);

	return 0;
}

#if defined(CONFIG_PM)
int
stm_bdisp2_aq_resume(struct stm_bdisp2_aq *aq)
{
	unsigned long                         next_valid_ip;

	/* Enable bdisp2 clocks */
	clk_prepare_enable(aq->bc->clk_ip);

    /* Set clock frequency */
	if (aq->bc->clk_ip_freq) {
		/* Should not get error, as this rate has
		 * been already set at boot time */
		clk_set_rate(aq->bc->clk_ip, aq->bc->clk_ip_freq);
	}

	/* Do a BDisp global soft reset on the first device */
	if (aq->stdev.queue_id == 0)
		bdisp2_engine_reset(&aq->stdrv, &aq->stdev);

	/* Enable the blitter IRQ */
	enable_irq(aq->irq);

	/* Set up the hardware -> point registers to right place */
	bdisp2_set_AQ_reg (&aq->stdrv, &aq->stdev,
			   BDISP_AQ_CTL,
			   (3 - aq->stdev.queue_id)
			   | (0
			      | BDISP_AQ_CTL_QUEUE_EN
			      | BDISP_AQ_CTL_EVENT_SUSPEND
			      | BDISP_AQ_CTL_IRQ_NODE_COMPLETED
			      | BDISP_AQ_CTL_IRQ_LNA_REACHED
			     )
			  );

	/* Setup the AQ iterrupt mask if needed */
	if (aq->stdev.features.hw.need_itm)
		bdisp2_set_reg (&aq->stdrv, BDISP_ITM0,
			        (BDISP_ITS_AQ_MASK << (BDISP_ITS_AQ1_SHIFT
			                               + (aq->stdev.queue_id * 4))));

	next_valid_ip = aq->stdev.bdisp_nodes.next_ip;

	/* Program AQ_IP with new next valid IP register value */
	bdisp2_set_AQ_reg (&aq->stdrv, &aq->stdev, BDISP_AQ_IP, next_valid_ip);

	/* Update the driver power state */
	aq->stdrv.bdisp_suspended = 0;

	/* We can now unlock the driver */
	UNLOCK_ATOMIC(&aq->stdev);

	return 0;
}
#endif /* CONFIG_PM */
