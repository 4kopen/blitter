/*
* This file is dual licensed, either GPLv2.0
* or LGPLv2.1, at your option.
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
********************************************************************************
*
* License type: GPLv2.0
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with it. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
* Alternatively, this may be distributed under the terms of
* LGPLv2.1, in which case the following provisions apply instead of the ones
* mentioned above :
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
*/

#ifndef __BDISPII_AQ_STATE_H__
#define __BDISPII_AQ_STATE_H__


#include <linux/types.h>
#ifdef __KERNEL__
#include <linux/bpa2.h>
#endif /* __KERNEL__ */
#include <linux/stm/blitter.h>
#include <linux/stm/bdisp2_nodegroups.h>
#include <linux/stm/bdisp2_features.h>

#include "state.h"

#include "bdisp2/bdispII_device_features.h"
#include "bdisp2/bdispII_driver_features.h"
#include "bdisp2/bdispII_aq_operations.h"


#include <linux/types.h>

enum stm_bdisp2_wait_type {
	STM_BDISP2_WT_IDLE,
	STM_BDISP2_WT_SPACE,
	STM_BDISP2_WT_FENCE,
};

enum bdisp2_palette_type {
	SG2C_NORMAL,
	SG2C_COLORALPHA              /* αout = colorα, RGBout = RGBin */,
	SG2C_INVCOLORALPHA           /* αout = 1-colorα, RGBout = RGBin */,
	SG2C_ALPHA_MUL_COLORALPHA    /* αout = α * colorα, RGBout = RGBin
					*/,
	SG2C_INV_ALPHA_MUL_COLORALPHA /* αout = 1-(α * colorα),
					RGBout = RGBin */,
	SG2C_DYNAMIC_COUNT           /* number of dynamic palettes */,

	SG2C_ONEALPHA_RGB      /* αout = 1, RGBout = RGBin */
							= SG2C_DYNAMIC_COUNT,
	SG2C_INVALPHA_ZERORGB  /* αout = 1 - Ain, RGBout = 0 */,
	SG2C_ALPHA_ZERORGB     /* αout = Ain, RGBout = 0 */,
	SG2C_ZEROALPHA_RGB     /* αout = 0, RGBout = RGBin */,
	SG2C_ZEROALPHA_ZERORGB /* αout = 0, RGBout = 0 */,
	SG2C_ONEALPHA_ZERORGB  /* αout = 1, RGBout = 0 */,

	SG2C_COUNT
};



#ifndef __KERNEL__
#  define __iomem
#endif /* __KERNEL__ */

struct stm_bdisp2_dma_area {
	struct bpa2_part      *part;
	unsigned long          base;
	size_t                 size;
	volatile void __iomem *memory;
	bool                   cached;
};

struct stm_bdisp2_nodes_list {
	struct stm_bdisp2_dma_area area;

	unsigned long          last_free; /* offset of last free node */
	unsigned long          next_free; /* offset of the node driver write to */
	unsigned long          next_ip;
	unsigned long          prev_set_lna; /* previously set LNA */
	unsigned long          nodes_phys;
	unsigned long          nodes_size;
	size_t                 usable_nodes_size; /* might not be a
                                       multiple of our sizeof(<biggest_node>) */
	unsigned int           num_ops_hi; /*64 bits node serial number*/
	unsigned int           num_ops_lo;
	uint32_t               node_irq_delay; /* every now and then,
	 we want to see a node completed interrupt, so a) we don't haveto wait for
	 a complete list to finish,and b) we don't want them to happen too often */
#ifdef __KERNEL__
	atomic_t               lock; /*a lock for concurrent access to node list */
	unsigned char          locked_by; /*0 : none,
                                        2 : kernel, 3 : ioctl push_nodes */
#endif
};

struct statistics {
	unsigned int  num_irqs; /* total number of IRQs (not only LNA + NODE) */
	unsigned int  num_lna_irqs;  /* total LNA IRQs */
	unsigned int  num_node_irqs; /* total node IRQs */
	unsigned int  num_idle;
	unsigned int  num_wait_idle;
	unsigned int  num_wait_next;
	unsigned int  num_starts;
};

struct stm_bdisp2_driver_data {
	int                         version;

	volatile void __iomem       *mmio_base; /*BDisp base address */

	unsigned char               num_pending_requests; /*DPM*/
	unsigned char               bdisp_suspended;  /* power status,
-	                                   0 = Device is running
-	                                   1 = Device is suspended by user
-	                                   2 = device is suspended by RPM */

	struct stm_bdisp2_dma_area  tables; /*!< CLUTs and filters */
	unsigned long __iomem       *clut_virt[SG2C_DYNAMIC_COUNT]; /* CLUTaddresses */
	unsigned long               last_lut;

	struct stm_plat_blit_data   plat_blit; /* BDisp configuration params */

	struct bdisp2_funcs         funcs;

	struct statistics           stat;

};

struct stm_bdisp2_device_setup {
	struct _BltNodeGroup00 ConfigGeneral;
	struct _BltNodeGroup01 ConfigTarget;
	struct target_loop {
		uint32_t BLT_TBA;
		uint32_t BLT_TTY;
		struct {
			uint16_t h; /*!< horizontal divider for x/w */
			uint16_t v; /*!< vertical divider for y/h */
		} coordinate_divider;
	} target_loop[2]; /*!< additional loops for planar targets,
			    loop[0] is Cb/Cr or Cb, loop[1] is Cr */
	unsigned int n_target_extra_loops;
	struct _BltNodeGroup02 ConfigColor;
	struct _BltNodeGroup03 ConfigSource1;
	struct _BltNodeGroup04 ConfigSource2;
	struct _BltNodeGroup05 ConfigSource3;
#ifdef BDISP2_SUPPORT_HW_CLIPPING
	struct _BltNodeGroup06 ConfigClip;
#endif
	struct _BltNodeGroup07 ConfigClut;
	struct _BltNodeGroup08 ConfigFilters;
	struct _BltNodeGroup09 ConfigFiltersChr;
	struct _BltNodeGroup10 ConfigFiltersLuma;
	struct _BltNodeGroup11 ConfigFlicker;
	struct _BltNodeGroup12 ConfigColorkey;
	struct _BltNodeGroup14 ConfigStatic;
	struct _BltNodeGroup15 ConfigIVMX;
	struct _BltNodeGroup16 ConfigOVMX;
	struct _BltNodeGroup18 ConfigVC1R;

	/* state shared between both draw and blit states */
	struct {
		__u32 dst_ckey[2];
		__u32 extra_blt_ins; /* for RGB32 to always enable plane mask */
		__u32 extra_blt_cic; /* for RGB32 to always enable plane mask */
	} all_states;

	/* drawing state */
	struct {
		struct _BltNodeGroup00 ConfigGeneral;
		__u32                  color;
		__u32                  color_ty;
		struct _BltNodeGroup07 ConfigClut;
	        struct _BltNodeGroup15 ConfigIVMX;
	} drawstate;

	/* blitting state */
	struct {
		long source_w; /* the total width of the surface in 16.16 */
		long source_h; /* the total height of the surface in 16.16 */

		unsigned int srcFactorH; /*!< H factor for source1 and source2
					    coordinate increment, will normally
					    be == 1. For certain YCbCr pixel
					    formats it will be == 2, though. */
		unsigned int srcFactorV; /*!< V factor for source1 and source2
					    coordinate increment, will normally
					    be == 1. For certain YCbCr pixel
					    formats it will be == 2, though. */

		__u32 src_ckey[2];

		unsigned int canUseHWInputMatrix:1;
		unsigned int isOptimisedModulation:1;
		unsigned int bIndexTranslation:1;
		unsigned int bFixedPoint:1;
		unsigned int srcxy_fixed_point:1;
		unsigned int src_premultcolor:1;
		unsigned int bDeInterlacingTop:1;
		unsigned int bDeInterlacingBottom:1;
		unsigned int flicker_filter:1;
		unsigned int strict_input_rect:1;
		unsigned int strict_filter_usage:1;

		int rotate; /*!< Counter clockwise degrees of rotation.
			      90 180 270 and some special internal values are
			      supported, only. */
		__u32 blt_ins_src1_mode;
		__u32 blt_fctl_rzc;

		int n_passes;
		int rsf_level;
		struct {
			struct _BltNodeGroup00   ConfigGeneral;
			enum bdisp2_palette_type palette_type;
		} extra_passes[2];
	} blitstate;

	enum bdisp2_palette_type      palette_type;
	struct stm_blitter_palette_s *palette;
	bool                          do_expand;
	unsigned int                  upsampled_src_nbpix_min;
	unsigned int                  upsampled_dst_nbpix_min;

	/* these are per operation */
	int h_trgt_sign, h_src2_sign, v_trgt_sign, v_src2_sign;
};

struct stm_bdisp2_device_data {
	struct stm_bdisp2_nodes_list  bdisp_nodes; /* node list */

	struct bdisp2_features features; /* hardware features */

	uint32_t               queue_id; /* id of queue */

	unsigned long long     bdisp_ops_start_time; /* perfs */
	unsigned long long     ticks_busy;           /* perfs */

	unsigned long clut_phys[SG2C_COUNT];
	unsigned long filter_8x8_phys;
	unsigned long filter_5x8_phys;

	uint32_t force_slow_path; /* force slow path copy / blit */
	uint32_t no_blend_optimisation; /* don't use fast direct fill/copy
	                                   during PorterDuff blends, even
                                       if possible. */

	/* state etc */
	int v_flags; /* validation flags */

	/* this is per (stretch blit) - they can change without changing the
	   hardware state */
	__u32 hsrcinc; /* in 16.16 */
	__u32 vsrcinc; /* in 16.16 */

	struct stm_bdisp2_device_setup _setup;
	struct stm_bdisp2_device_setup *setup;

#ifdef STGFX2_CLUT_UNSAFE_MULTISESSION
	unsigned int clut_disabled:1;
#endif
};


enum stm_blitter_accel
bdisp2_state_supported(const struct stm_bdisp2_device_data *stdev,
		       const struct stm_blitter_state      *state,
		       enum stm_blitter_accel               accel);

void bdisp2_state_update(struct stm_bdisp2_driver_data *stdrv,
			 struct stm_bdisp2_device_data *stdev,
			 struct stm_blitter_state      *state,
			 enum stm_blitter_accel         accel);

int
bdisp2_check_memory_constraints(const struct stm_bdisp2_device_data * const stdev,
				unsigned long                        start,
				unsigned long                        end);


#endif /* __BDISPII_AQ_STATE_H__ */
