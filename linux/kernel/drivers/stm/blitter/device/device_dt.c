/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <linux/clkdev.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/clk-private.h>

#ifdef CONFIG_SUPERH
#include <asm/irq-ilc.h>
#endif

#ifdef CONFIG_ARM
#include <asm/mach-types.h>
#endif


#include "bdisp2/bdisp2_os.h"
#include "blit_debug.h"
#include "blitter_device.h"
#include "device/device_dt.h"

/*
 * Define the maximum number of clocks the blitter driver can be parsing from
 * the DT node.
 * Current BDispII IP version is using only 3 clocks ; two are shared (ic_clk
 * and cpu_clk) and one is owned by the bdisp IP (bdisp_clk).
 *
 * The driver is only taking care of IP and ICN clocks; CPU clock is not
 * managed in the blitter driver.
 */

extern struct of_device_id stm_blitter_match[];

static void __init stm_blit_dt_dump(struct device_node        *np,
			            struct stm_plat_blit_data *data)
{
	const char *nameReq;
	const char *nameComp = NULL;
	int index = 0;
	int ret = 0;

	/* Get the first requested compatible field */
	ret = of_property_read_string_index(np, "compatible", 0, &nameReq);
	if (ret != 0)
		stm_blit_printe("No Compatible field");

	while (strnlen(stm_blitter_match[index].compatible,sizeof(stm_blitter_match[index].compatible)-1)) {
		if ((enum stm_blitter_device_type)stm_blitter_match[index].data
		      == data->device_type) {
			nameComp = stm_blitter_match[index].compatible;
			break;
		}
		index++;
	}

	/* Print-out used version of Blitter  */
	stm_blit_printd(0,
			"Required stm-blitter     : %s\n",
			nameReq);

	if (nameComp != NULL)
	{
		if (strcmp(nameComp, nameReq) != 0)
			stm_blit_printi("Device NOT FOUND using compatible: %s (%d)\n",
					nameComp, data->device_type);
		else
			stm_blit_printd(0,
					"Using stm-blitter device : %s (%d)\n",
					nameComp, data->device_type);
	}

	stm_blit_printd(0,
			"AQs (%d) CQs (%d)\n",
			data->nb_aq, data->nb_cq);
	stm_blit_printd(0,
			"Line delay (%d) MB delay (%d) Rotation delay (%d)\n",
			data->line_buffer_length, data->mb_buffer_length,
			data->rotate_buffer_length);
}

#ifndef CONFIG_ARCH_STI
int __init stm_blit_dt_get_compatible(struct device_node *np)
{
	int index;
	int index2;
	const char *name;

	/* Loop over all valid compatibility string */
	index = 0;
	while (of_property_read_string_index(np, "compatible", index, &name)
	       == 0) {
		/* Lock for compatibility in the compatible table */
		index2 = 0;
		while (strnlen(stm_blitter_match[index2].compatible,sizeof(stm_blitter_match[index2].compatible)-1)) {
			if (strcmp(stm_blitter_match[index2].compatible, name)
			    == 0) {
				/* We get request compatibility, return */
				return (enum stm_blitter_device_type)
					stm_blitter_match[index2].data;
			}
			index2++;
		}
		index++;
	}

	/* This should never happen, there should
	   always found a compatible device node */
	BUG_ON(true);
	return -EINVAL;
}
#endif

int __init stm_blit_dt_get_pdata(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	const struct of_device_id *device_id;
	struct stm_plat_blit_data *data;
	int ret;

	stm_blit_printd(0, "%s\n", __func__);

	pdev->dev.platform_data = NULL;

	/* Allocate memory space for platform data */
	data = devm_kzalloc(&pdev->dev, sizeof(*data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	/* Get device type that has probed this */
	device_id = of_match_device(stm_blitter_match, &pdev->dev);
	data->device_type = (enum stm_blitter_device_type)device_id->data;

	/* Read node properties */
	ret = of_property_read_u32(np, "aq", &data->nb_aq);
	if (ret != 0)
		return -EINVAL;

	ret = of_property_read_u32(np, "cq", &data->nb_cq);
	if (ret != 0)
		return -EINVAL;

	ret = of_property_read_u32(np, "line_buffer_length",
				   &data->line_buffer_length);
	if (ret != 0)
		return -EINVAL;

	ret = of_property_read_u32(np, "rotate_buffer_length",
				   &data->rotate_buffer_length);
	if (ret != 0)
		return -EINVAL;

	ret = of_property_read_u32(np, "mb_buffer_length",
				   &data->mb_buffer_length);
	if (ret != 0)
		return -EINVAL;

	ret = of_property_read_u32(np, "clock-frequency",
				   &data->clk_ip_freq);
	if (ret != 0)
		/* Optional, otherwise will use default value */
		data->clk_ip_freq = 0;

	data->hw_instance_id = of_alias_get_id(np, "blitter");

	snprintf(data->device_name, sizeof(data->device_name),
			 "stm-bdispII.%01d", of_alias_get_id(np, "blitter"));

	pdev->dev.platform_data = (void *)(uintptr_t)data;

	stm_blit_dt_dump(np, data);

	return 0;
}

void __exit stm_blit_dt_put_pdata(struct platform_device *pdev)
{
	struct stm_plat_blit_data *data
	   = (struct stm_plat_blit_data *) pdev->dev.platform_data;

	stm_blit_printd(0, "%s\n", __func__);

	devm_kfree(&pdev->dev, data);

	pdev->dev.platform_data = NULL;
}

int __init stm_blit_dt_init(void)
{
	return 0;
}

int stm_blit_dt_exit(void)
{
	struct device_node *np;

	stm_blit_printd(0, "%s\n", __func__);

	np = of_find_matching_node(NULL, stm_blitter_match);
	if (!np)
		return -ENODEV;

	return 0;
}
