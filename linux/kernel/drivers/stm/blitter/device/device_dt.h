/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __DEVICE_DT_H__
#define __DEVICE_DT_H__

int stm_blit_dt_get_pdata(struct platform_device *pdev);

void stm_blit_dt_put_pdata(struct platform_device *pdev);

int stm_blit_dt_init(void);

int stm_blit_dt_exit(void);

#endif /* __DEVICE_DT_H__ */
