/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __STM_BLIT_STRINGS_H__
#define __STM_BLIT_STRINGS_H__


const char *
stm_blitter_get_surface_format_name(stm_blitter_surface_format_t fmt);
const char *
stm_blitter_get_porter_duff_name(stm_blitter_porter_duff_rule_t pd);

char *
stm_blitter_get_accel_string(enum stm_blitter_accel accel);
char *
stm_blitter_get_modified_string(enum stm_blitter_state_modification_flags mod);
char *
stm_blitter_get_drawflags_string(stm_blitter_surface_drawflags_t flags);
char *
stm_blitter_get_blitflags_string(stm_blitter_surface_blitflags_t flags);


#endif /* __STM_BLIT_STRINGS_H__ */
