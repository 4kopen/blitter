/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __BDISPII_AQ_FEATURES_H__
#define __BDISPII_AQ_FEATURES_H__

#include <linux/types.h>


struct bdisp2_pixelformat_info {
	enum stm_blitter_surface_format_e stm_blitter_format;
	__u32 bdisp_type; /* BLIT_COLOR_FORM_RGB565 etc. */
	unsigned int supported_as_src:1;
	unsigned int supported_as_dst:1;
};

extern const struct bdisp2_pixelformat_info stm_blit_to_bdisp_template[];

struct _stm_bdisp2_hw_features {
	__u16 size_mask;
	__u16 line_buffer_length;
	__u16 mb_buffer_length;
	__u16 rotate_buffer_length;
	__u16 upsampled_nbpix_src_min;
	__u16 upsampled_nbpix_dst_min;

	/* see https://bugzilla.stlinux.com/show_bug.cgi?id=19843#c1 */
	unsigned int nmdk_macroblock:1; /*!< nomadik macroblock support */
	unsigned int planar_r:1; /*!< can read 2 & 3 planar surfaces */
	unsigned int planar2_w:1; /*!< can write 2-planar surfaces */
	unsigned int planar3_w:1; /*!< can write 3-planar surfaces */
	unsigned int planar2_10b_r:1; /*!< can read 2-planar 10 bits surfaces */
	unsigned int rgb32:1; /*!< 4byte xRGB  */
	unsigned int rle_bd:1; /*!< RLE (BD) decoding */
	unsigned int rle_hd:1; /*!< RLE (HD-DVD) decoding */
	unsigned int s1_422r:1; /*!< 422 raster supported on S1 */
	unsigned int s1_subbyte:1; /*!< subbyte formats on S1 */

	unsigned int boundary_bypass:1; /*!< boundary bypass */
	unsigned int dst_colorkey:1; /*!< working destination color keying */
	unsigned int flicker_filter:1; /*!< flicker filter */
	unsigned int gradient:1; /*!< support gradient fill */
	unsigned int owf:1; /*!< hw support for OWF blending */
	unsigned int plane_mask:1; /*!< hw support for plane mask */
	unsigned int porterduff:1; /*!< hw support for PorterDuff blending */
	unsigned int porterduff_extend:1; /*!< hw support for new PorterDuff rules */
	unsigned int secure_operation:1; /*!< hw support for Secure operation */
	unsigned int rotation:1; /*!< support rotation */
	unsigned int spatial_dei:1; /*!< spatial DEI */
	unsigned int nvxx_10b_swapped_chroma:1; /*!< swapped chroma for nvxx_10B formats */

	unsigned int no_address_banks:1; /*!< linear memory access, not in
					banks of 64MB  */
	unsigned int size_4k:1; /*!< 4Ki sizes supported (x/y: -8192...8191
				w/h: 0...8191) */

	unsigned int aq_lock:1; /*!< lock node feature for AQs */
	unsigned int disable_clk_gating:1; /*!< Disable the clock gating */

	unsigned int write_posting:1; /*!< write posting on target plug */
	unsigned int need_itm:1; /*!< route interrupts to girq0 */
};

struct bdisp2_features {
	struct _stm_bdisp2_hw_features hw;

	struct bdisp2_pixelformat_info stm_blit_to_bdisp[STM_BLITTER_SF_COUNT];

	enum stm_blitter_surface_drawflags_e drawflags;
	enum stm_blitter_surface_blitflags_e blitflags;
};
#endif /* __BDISPII_AQ_FEATURES_H__ */
